module.exports = function (w) {
    return {
      files: [
        'src/*.ts',
        'demos/*.ts'
      ],

      tests: [
        'test/*.test.ts',
        'demos/test/*.test.ts'
      ],

      env: {
        type: 'node'
      },

      compilers: {
        '**/*.ts': w.compilers.typeScript({ module: 'commonjs' })
      }
    };
  };
