// declare an interface
export interface IBook {}

// Using interface as a contract
const book = {
    title: 1234,
    reference: "JavaScript",
};