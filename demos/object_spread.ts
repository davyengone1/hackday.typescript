export const book1 = {
    title: "JavaScript rokcs",
    author: "Tonio",
    publisher: "Hackages",
};

// Let's create another book by the same author
export const book2 = {
    title: "TypeScript rocks!",
    author: book1.author,
    publisher: "Hackages",
};

// How about using object destructuring
