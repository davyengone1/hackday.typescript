# Typescript Koans

In this set of examples and tests we cover typescript concepts by building a simple and lighter version of Lodash

## Quick start

### Prerequisites

- **Make sure to have Node version >= [LTS](https://nodejs.org) and NPM >= 5 or Yarn > 1.5**
- **Make sure to have git installed on your computer**

```bash
# clone the repo
git clone https://github.com/hackages/hackjam.typescript.git

# change directory to the repo
cd hackday.typescript

# start the server (`npm install` will be performed for you)
npm start or yarn start

# start your tests in the console (`npm install` will be performed for you)
npm test

```

go to [http://localhost:8080](http://localhost:8080) in your browser

## Testing (Test Driven Development)

We use [Mocha](https://mochajs.org) in this repository

**If you're using [`yarn`](https://yarnpkg.com/en), make sure to remove the `prestart` script before running `yarn**

**If you're using [Wallaby](https://wallabyjs.com), just run it and you'll be on your way to master TS syntax**

### TypeScript

> To take full advantage of TypeScript with autocomplete you would have to install it globally and use an editor with the correct TypeScript plugins.

#### Use the latest TypeScript compiler

TypeScript 2.7.x includes everything you need. Make sure to upgrade, even if you installed TypeScript previously.

```bash
npm install --global typescript@latest
```

#### Use a TypeScript-aware editor

We recommend to use [Visual Studio Code](https://code.visualstudio.com/) but feel free to use your favourite Code Editor.

We also have good experience using these editors:

* [Webstorm](https://www.jetbrains.com/webstorm/download/)
* [Atom](https://atom.io/) with [TypeScript plugin](https://atom.io/packages/atom-typescript)

## Contributing

Feel free to send us PRs

Happy coding!

[Hackages Team](http://hackages.io)
