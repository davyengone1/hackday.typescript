# Roadmap of the training

## Getting started

- Installation and configuration
- Using static typing and type inference
- Union types and types alias
- Interface vs Class
- Inheritance
- Using Assertion to Convert Types
- Generics

## Decorators Demystified

- Decorator Factories
- Class Method, Property and Parameter Decorators

## TypeScript Definition/Declaration Files

- .d.ts Files in nutshell
- @types and DefinitelyTyped

## Enums

- Demystifying it
- Ambient enums

## Intro to TypeScript Compiler

- tsconfig.json
- Input files compilation management
- Emitting declaration and source-map files
- Type safety of the compiler

## Advanced TypeScript Compiler

- Module, target and resolution strategy
- Module scoping and the module option
- Configuring compiler CLI output

## Advanced output controls

- Advanced Type Constructs
- Intersection Types
- Union Types
- Type Guards and Differentiating Types
- Nullable types
- Type Aliases
- String Literal Types
- Typed this
- Index/Mapped types

## Declaration Merging

- Merging interfaces
- Module augmentation